
form_api = document.getElementById('form_api')
select_payvalida = document.getElementById('select_payvalida')
email_payvalida = document.getElementById('email_payvalida')
referencia_payvalida = document.getElementById('referencia_payvalida')
responde = document.getElementById('api_Respond')
json_body = {}
select_payvalida.onchange = function () {
    json_body = {}
    responde.classList.add('none')
    responde.innerHTML = ""
    email_payvalida.value = ""
    referencia_payvalida.value = ""
    form_api.setAttribute('select', this.value)
}

function validate_alert(e) {
    if (e.value.split(" ").join('') == "") {
        alert("Debe rellenar los campos")
        return false
    }
    return true
}

function validate_api() {
    switch (select_payvalida.value) {
        case "email_payvalida":
            json_body.email = email_payvalida.value
            return validate_alert(email_payvalida)
            break;
        case "referencia_payvalida":
            json_body.referencia = referencia_payvalida.value
            return validate_alert(referencia_payvalida)
            break;
        default:
            return true;
            break;
    }
    return true;
}

function show_respode(r, error = 0) {
    responde.innerHTML = ""
    responde.classList.add('none')
    if (error == 1) {
        console.log(r)
        responde.innerHTML += `
        <h3 class="">
            Ha ocurrido un Error
        </h3>
        <p>
            No fue posible realizar la consulta. Comprueba los datos ingresados
        </p>
        `
        responde.classList.remove('none')
        form_api.classList.remove('loader')
        return
    }
    e = JSON.parse(r)
    if (e.CODE != "0000") {
        responde.innerHTML = `
        <h3 class="">
            Ha ocurrido un Error
        </h3>
        <p>
            ${e.DESC}
        </p>
        `
        responde.classList.remove('none')
        form_api.classList.remove('loader')
        return
    }
    for (var i = 0; i < e.DATA.length; i++) {
        result = `
            <h3 class="">
                Resultados
            </h3>
            <table>
        `
        ele = Object.entries(e.DATA[i])
        for (var j = 0; j < ele.length; j++) {
            result += `
                <tr>
                    <td class="key">
                        ${ele[j][0]}
                    </td>
                    <td class="value">
                        ${ele[j][1]}
                    </td>
                </tr>
            `
        }
        result += `
            </table>
        `
        responde.innerHTML += result
    }
    responde.classList.remove('none')
    form_api.classList.remove('loader')
}

function send_api() {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Cookie", "__cfduid=dd176ec8bd918186258d0349983a6ca9b1594948465");
    form_api.classList.add('loader')
    var raw = JSON.stringify(json_body);
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch("https://api.payvalida.com/services/autogestion/consulta", requestOptions)
        .then(response => response.text())
        .then(result => show_respode(result))
        .catch(error => show_respode(error, 1));
}

function load_function() {
    btn = document.documentElement.querySelector('.elementor-widget-For_autoGestion_PAYVALIDA .form_api [type="submit"]')
    if (btn == null) return;
    btn.onclick = function (e) {
        e.preventDefault()
        if (!validate_api()) return;
        send_api()
    }
}
load_function()