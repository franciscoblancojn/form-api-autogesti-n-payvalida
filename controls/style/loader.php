<?php
    function loader_Form_API_autoGestion_PAYVALIDA($this_)
    {
        $this_->start_controls_section(
            'loader',
            [
                'label' => __( 'Loader' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
            $this_->add_control(
                'loader_color',
                [
                    'label' => __( 'Loader Color', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} .loader:after' => 'border-left-color: {{VALUE}};border-right-color: {{VALUE}};border-bottom-color: {{VALUE}}',
                    ],
                ]
            );
        $this_->end_controls_section();
    }