<?php
    function respond_Form_API_autoGestion_PAYVALIDA($this_){
        $this_->start_controls_section(
            'respond',
            [
                'label' => __( 'Respond', 'plugin-name' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        $this_->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'respond_title_typography',
                'label' => __( 'Title Typography', 'plugin-domain' ),
                'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} h3',
            ]
        );
        $this_->add_control(
            'title-color-table',
            [
                'label' => __( 'Title Color', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Scheme_Color::get_type(),
                    'value' => \Elementor\Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} h3' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this_->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'key_respond_typography',
                'label' => __( 'Key Typography', 'plugin-domain' ),
                'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .key',
            ]
        );
        $this_->add_control(
            'key-color-table',
            [
                'label' => __( 'Key Color', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Scheme_Color::get_type(),
                    'value' => \Elementor\Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .key' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this_->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'respond_typography',
                'label' => __( 'Typography', 'plugin-domain' ),
                'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} #api_Respond,{{WRAPPER}} table,{{WRAPPER}} tr,{{WRAPPER}} td',
            ]
        );
        $this_->add_control(
            'color-table',
            [
                'label' => __( 'Color', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Scheme_Color::get_type(),
                    'value' => \Elementor\Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} #api_Respond' => 'color: {{VALUE}}',
                    '{{WRAPPER}} table' => 'color: {{VALUE}}',
                    '{{WRAPPER}} tr' => 'color: {{VALUE}}',
                    '{{WRAPPER}} td' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this_->start_controls_tabs(
			'Separacion'
        );
        
        $this_->start_controls_tab(
			's_interna_tab',
			[
				'label' => __( 'Separacion Interna', 'plugin-name' ),
			]
		);
        $this_->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
            [
                'name' => 's_interna',
                'label' => __( 'Separacion Interna', 'plugin-domain' ),
                'fields_options' => [
                    'border' => ['default' => 'solid'],
                    'width' => [
                        'default' => [
                            'top' => 0,
                            'right' => 0,
                            'bottom' => 1,
                            'left' => 0,
                            'unit'=> 'px', 
                            'isLinked' => true,
                        ],
                    ],
                    'color' => ['default' => '#e4e4e4'],
                ],
                'selector' => '{{WRAPPER}} tr:not(:last-child)',
            ]
        );
        $this_->end_controls_tab();

        $this_->start_controls_tab(
			's_externa_tab',
			[
				'label' => __( 'Separacion Externa', 'plugin-name' ),
			]
		);
        $this_->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
            [
                'name' => 's_externa',
                'label' => __( 'Separacion Externa', 'plugin-domain' ),
                'fields_options' => [
                    'border' => ['default' => 'dashed'],
                    'width' => [
                        'default' => [
                            'top' => 0,
                            'right' => 0,
                            'bottom' => 3,
                            'left' => 0,
                            'unit'=> 'px', 
                            'isLinked' => true,
                        ],
                    ],
                    'color' => ['default' => '#e4e4e4'],
                ],
                'selector' => '{{WRAPPER}} table:not(:last-child):after',
            ]
        );
        $this_->end_controls_tab();
        
        $this_->end_controls_section();
    }