<?php
    function inputs_Form_API_autoGestion_PAYVALIDA($this_){
        $this_->start_controls_section(
            'inputs',
            [
                'label' => __( 'Inputs' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
            $this_->add_group_control(
                \Elementor\Group_Control_Typography::get_type(),
                [
                    'name' => 'input_typography',
                    'label' => __( 'Typography', 'plugin-domain' ),
                    'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} input,{{WRAPPER}} select',
                ]
            );
            $this_->add_control(
                'border-color-input',
                [
                    'label' => __( 'Border Color', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} input' => 'border-color: {{VALUE}}',
                        '{{WRAPPER}} select' => 'border-color: {{VALUE}}',
                    ],
                ]
            );
            $this_->add_control(
                'background-color-input',
                [
                    'label' => __( 'Background Color', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} input' => 'background-color: {{VALUE}}',
                        '{{WRAPPER}} select' => 'background-color: {{VALUE}}',
                    ],
                ]
            );
            $this_->add_control(
                'color-input',
                [
                    'label' => __( 'Color', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} input' => 'color: {{VALUE}}',
                        '{{WRAPPER}} input::placeholder' => 'color: {{VALUE}}',
                        '{{WRAPPER}} select' => 'color: {{VALUE}}',
                    ],
                ]
            );
        $this_->end_controls_section();
    }