<?php
function title_Form_API_autoGestion_PAYVALIDA($this_){
    $this_->start_controls_section(
        'title',
        [
            'label' => __( 'Title' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    );
        $this_->add_control(
            'title_color',
            [
                'label' => __( 'Title Color', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Scheme_Color::get_type(),
                    'value' => \Elementor\Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} h1' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this_->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => __( 'Typography', 'plugin-domain' ),
				'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} h1',
			]
		);
    $this_->end_controls_section();
}