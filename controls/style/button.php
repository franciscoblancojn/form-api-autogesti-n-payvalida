<?php
    function button_Form_API_autoGestion_PAYVALIDA($this_){
        $this_->start_controls_section(
            'button',
            [
                'label' => __( 'Button' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
            $this_->add_control(
                'background-color-button',
                [
                    'label' => __( 'Background Color', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} button' => 'background-color: {{VALUE}}',
                    ],
                ]
            );
            $this_->add_control(
                'color-button',
                [
                    'label' => __( 'Color', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} button' => 'color: {{VALUE}}',
                    ],
                ]
            );
            $this_->add_control(
                'background-color-button-hover',
                [
                    'label' => __( 'Background Color Hover', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} button:hover' => 'background-color: {{VALUE}}',
                    ],
                ]
            );
            $this_->add_control(
                'color-button-hover',
                [
                    'label' => __( 'Color hover', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} button:hover' => 'color: {{VALUE}}',
                    ],
                ]
            );
        $this_->end_controls_section();
    }