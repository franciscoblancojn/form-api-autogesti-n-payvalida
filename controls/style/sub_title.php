<?php
    function sub_title_Form_API_autoGestion_PAYVALIDA($this_){
        $this_->start_controls_section(
            'sub-title',
            [
                'label' => __( 'Sub-Title' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
            $this_->add_control(
                'sub_title_color',
                [
                    'label' => __( 'Sub-Title Color', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'scheme' => [
                        'type' => \Elementor\Scheme_Color::get_type(),
                        'value' => \Elementor\Scheme_Color::COLOR_1,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} h2' => 'color: {{VALUE}}',
                    ],
                ]
            );
            $this_->add_group_control(
                \Elementor\Group_Control_Typography::get_type(),
                [
                    'name' => 'sub_title_typography',
                    'label' => __( 'Typography', 'plugin-domain' ),
                    'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} h2',
                ]
            );
        $this_->end_controls_section();
    }