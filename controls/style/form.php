<?php
function form_Form_API_autoGestion_PAYVALIDA($this_){
    $this_->start_controls_section(
        'Form',
        [
            'label' => __( 'Form' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    );
        $this_->add_control(
            'alignment',
            [
                'label' => __( 'Alignment' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'flex-start' => [
                        'title' => __( 'Left', 'plugin-domain' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'plugin-domain' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'flex-end' => [
                        'title' => __( 'Right', 'plugin-domain' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'center',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}} .content_wi' => 'justify-content: {{VALUE}}',
                ],
            ]
        );
        $this_->add_group_control(
            \Elementor\Group_Control_Background::get_type(),
            [
                'name' => 'background',
                'label' => __( 'Background', 'plugin-domain' ),
                'types' => [ 'classic', 'gradient', 'video' ],
                'selector' => '{{WRAPPER}} .box_c',
            ]
        );
    $this_->end_controls_section();
}