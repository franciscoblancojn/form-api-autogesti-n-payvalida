<?php
/*
Plugin Name: Form API autoGestión PAYVALIDA
Plugin URI: 
Description: Genera un widget para elemento que proporciona un formulario con la funcionalidad de autoGestion de Payvalida
Version: 1
Author: Startscoinc
Author URI: https://startscoinc.com/es/
 */

function Form_API_autoGestion_PAYVALIDA_f()
{
    class FormAutoGestionPayvalida extends \Elementor\Widget_Base {

        /**
         * Get widget name.
         *
         * Retrieve oEmbed widget name.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget name.
         */
        public function get_name() {
            return 'For_autoGestion_PAYVALIDA';
        }
    
        /**
         * Get widget title.
         *
         * Retrieve oEmbed widget title.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget title.
         */
        public function get_title() {
            return __( 'Form autoGestion PAYVALIDA' );
        }
    
        /**
         * Get widget icon.
         *
         * Retrieve oEmbed widget icon.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget icon.
         */
        public function get_icon() {
            return 'eicon-form-horizontal';
        }
    
        /**
         * Get widget categories.
         *
         * Retrieve the list of categories the oEmbed widget belongs to.
         *
         * @since 1.0.0
         * @access public
         *
         * @return array Widget categories.
         */
        public function get_categories() {
            return [ 'general' ];
        }
    
        /**
         * Register oEmbed widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         * @access protected
         */
        protected function _register_controls() {
            $this->start_controls_section(
                'content_section',
                [
                    'label' => __( 'Content' ),
                    'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                ]
            );
    
            $this->add_control(
                'Title',
                [
                    'label' => __( 'Title' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'placeholder' => __( 'Title' ),
                    'default' => __('Consulta el estado de tu compra')
                ]
            );
            $this->add_control(
                'Text_Select',
                [
                    'label' => __( 'Texto Select' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'placeholder' => __( 'Texto del input Select' ),
                    'default' => __('Por favor selecciona un metodo de consulta')
                ]
            );
    
            $this->add_control(
                'Label_Email',
                [
                    'label' => __( 'Label Email' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'placeholder' => __( 'Texto de label Email' ),
                    'default' => __('Ingresa tu Email')
                ]
            );
            $this->add_control(
                'Placeholder_Email',
                [
                    'label' => __( 'Placeholder Email' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'placeholder' => __( 'Placeholder de Email' ),
                    'default' => __('Email')
                ]
            );
            $this->add_control(
                'Label_referencia',
                [
                    'label' => __( 'Label Referencia' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'placeholder' => __( 'Texto de label referencia' ),
                    'default' => __('Ingresa Referencia')
                ]
            );
            $this->add_control(
                'Placeholder_referencia',
                [
                    'label' => __( 'Placeholder Referencia' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'placeholder' => __( 'Placeholder de Referencia' ),
                    'default' => __('Numero de Referencia')
                ]
            );
            $this->add_control(
                'Text_btn',
                [
                    'label' => __( 'Button Text' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'placeholder' => __( 'Text of Button ' ),
                    'default' => __('Consultar')
                ]
            );
    
            $this->end_controls_section();
            
            $controls = ["form","title","sub_title","loader","inputs","button","respond"];
            for ($i=0; $i < count($controls); $i++) { 
                require plugin_dir_path(__FILE__).'controls/style/'.$controls[$i].'.php';
                eval($controls[$i]."_Form_API_autoGestion_PAYVALIDA(\$this);");
            }
        }
    
        /**
         * Render oEmbed widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         * @access protected
         */
        protected function render() {
            $settings = $this->get_settings_for_display();
            ?>
<link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ )."assets/css/style_widget.css"?>">
<div class="content_wi">
    <div>
        <h1>
            <?php echo $settings['Title'];?>
        </h1>
        <section id="form_api" class='box_c form_api' select="email_payvalida">
            <form method="post" name="Form_api">
                <div class="content_file">
                    <div class="content_select">
                        <h2 class="sub-title">
                            <?php echo $settings['Text_Select'];?>
                        </h2>
                        <select name="select_payvalida" id="select_payvalida">
                            <option value="email_payvalida">Email</option>
                            <option value="referencia_payvalida">Referencia</option>
                        </select>
                    </div>
                    <div class="content_input email_payvalida">
                        <label for="form-field-cedula">
                            <?php echo  $settings['Label_Email'];?>
                        </label>
                        <input size="1" type="text" name="email" id="email_payvalida"
                            placeholder="<?php echo  $settings['Placeholder_Email'];?>">
                    </div>
                    <div class="content_input referencia_payvalida">
                        <label for="form-field-referencia_pago">
                            <?php echo  $settings['Label_referencia'];?>
                        </label>
                        <input size="1" type="number" name="referencia" id="referencia_payvalida"
                            placeholder="<?php echo  $settings['Placeholder_referencia'];?>">
                    </div>
                    <div class="content_submit">
                        <button type="submit">
                            <?php echo  $settings['Text_btn'];?>
                        </button>
                    </div>
                </div>
            </form>
        </section>
        <section id="api_Respond" class="box_c curbe_s none">
        </section>
    </div>
</div>
<script src="<?php echo plugin_dir_url( __FILE__ )."assets/js/main.js"?>"></script>
<?php
        }
    
    }
}
add_action( 'elementor_pro/init', function() {
    Form_API_autoGestion_PAYVALIDA_f();
    \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \FormAutoGestionPayvalida() );
});